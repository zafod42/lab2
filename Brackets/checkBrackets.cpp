#include "checkBrackets.h"
#include "../Stack/fixedStack.hpp"

bool isOpposite(char open, char close)
{
    if (open == '(' && close == ')') return true;
    if (open == '[' && close == ']') return true;
    if (open == '{' && close == '}') return true;

    return false;
}

bool isCloseBracket(char ch)
{
    switch (ch)
    {
    case ')':
    case ']':
    case '}':
        return true;
    default:
        return false;
    }
}

bool isOpenBracket(char ch)
{
    switch (ch)
    {
    case '(':
    case '[':
    case '{':
        return true;
    default:
        return false;
    }
}

int countBrackets(const char* text)
{
    int counter = 0;
    int i = 0;
    while (text[i] != '\0')
    {
        if (isOpenBracket(text[i]))
        {
            ++counter;
        }
        ++i;
    }
    return counter;
}

bool checkBalanceBrackets(const char* text, int maxDeep)
{
    int max_size = countBrackets(text);
    FixedStack<char> checker(max_size);

    int j = 0;
    while (text[j] != '\0')
    {
        if (isOpenBracket(text[j]))
        {
            if (checker.getSize() >= maxDeep)
            {
                return false;
            }
            checker.push(text[j]);
        }
        else if (isCloseBracket(text[j]))
        {
            if (checker.isEmpty())
            {
                return false;
            }
            char tmp = checker.pop();
            if (!isOpposite(tmp, text[j]))
            {
                checker.push(tmp);
            }
        }
        ++j;
    }
    if (checker.isEmpty()) return true;

    return false;
}