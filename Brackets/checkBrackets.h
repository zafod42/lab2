#pragma once

int countBrackets(const char* text);
bool checkBalanceBrackets(const char* text, int maxDeep);
bool isOpenBracket(char ch);
bool isCloseBracket(char ch);
bool isOpposite(char open, char close);