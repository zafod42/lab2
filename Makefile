TARGET := lab2
PREFIX := /usr/local/bin
SRC := main.cpp ./Brackets/checkBrackets.cpp
OBJ := $(SRC:.cpp=.o)

CPP := g++ -o 

.PHONY: all clean install uninstall

all: $(TARGET)

main.o: main.cpp
	$(CPP) main.o -c main.cpp

brackets.o: ./Brackets/checkBrackets.cpp
	$(CPP) brackets.o -c ./Brackets/checkBrackets.cpp

$(TARGET): $(OBJ)
	$(CPP) $(TARGET) $(OBJ)

clean:
	rm -rf $(TARGET) $(OBJ) 

install:
	install $(TARGET) $(PREFIX)

uninstall:
	rm -rf $(PREFIX)/$(TARGET)
