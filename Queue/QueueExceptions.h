#include <string>
#include <exception>

#ifndef __QUEUEUNDERFLOW_H
#define __QUEUEUNDERFLOW_H


class QueueUnderflow : public std::exception
{
private:
    std::string msg;
public:
    QueueUnderflow(const std::string& m) noexcept : msg(m) {};
    QueueUnderflow(std::string&& m) noexcept : msg(std::move(m)) {};

    ~QueueUnderflow() noexcept = default;

    const char* what() const noexcept override
    {
        return msg.c_str();
    }
};

#endif // __QUEUEUNDERFLOW_H

#ifndef __QUEUEOVERFLOW_H
#define __QUEUEOVERFLOW_H


class QueueOverflow : public std::exception
{
private:
    std::string msg;
public:
    QueueOverflow(const std::string& m) noexcept : msg(m) {};
    QueueOverflow(std::string&& m) noexcept : msg(std::move(m)) {};

    ~QueueOverflow() noexcept = default;

    const char* what() const noexcept override
    {
        return msg.c_str();
    }
};

#endif // __QUEUEUNDERFLOW_H

#ifndef __WRONGQUEUESIZE_H
#define __WRONGQUEUESIZE_H


class WrongQueueSize : public std::exception
{
private:
    std::string msg;
public:
    WrongQueueSize(const std::string& m) noexcept : msg(m) {};
    WrongQueueSize(std::string&& m) noexcept : msg(std::move(m)) {};

    ~WrongQueueSize() noexcept = default;

    const char* what() const noexcept override
    {
        return msg.c_str();
    }
};

#endif // __WRONGQUEUESIZE_h