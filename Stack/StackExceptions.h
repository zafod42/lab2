#include <string>
#include <exception>

#ifndef __STACKUNDERFLOW_H
#define __STACKUNDERFLOW_H


class StackUnderflow : public std::exception
{
private:
    std::string msg;
public:
    StackUnderflow(const std::string& m) noexcept : msg(m) {};
    StackUnderflow(std::string&& m) noexcept : msg(std::move(m)) {};

    ~StackUnderflow() noexcept = default;

    const char* what() const noexcept override
    {
        return msg.c_str();
    }
};

#endif // __STACKUNDERFLOW_H

#ifndef __STACKOVERFLOW_H
#define __STACKOVERFLOW_H


class StackOverflow : public std::exception
{
private:
    std::string msg;
public:
    StackOverflow(const std::string& m) noexcept : msg(m) {};
    StackOverflow(std::string&& m) noexcept : msg(std::move(m)) {};

    ~StackOverflow() noexcept = default;

    const char* what() const noexcept override
    {
        return msg.c_str();
    }
};

#endif // __STACKUNDERFLOW_H

#ifndef __WRONGSTACKSIZE_H
#define __WRONGSTACKSIZE_H


class WrongStackSize : public std::exception
{
private:
    std::string msg;
public:
    WrongStackSize(const std::string& m) noexcept : msg(m) {};
    WrongStackSize(std::string&& m) noexcept : msg(std::move(m)) {};

    ~WrongStackSize() noexcept = default;

    const char* what() const noexcept override
    {
        return msg.c_str();
    }
};

#endif // __WRONGSTACKSIZE_h