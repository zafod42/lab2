#include "Stack.h"
#include "StackExceptions.h"
#include <iostream>

#ifndef __FIXEDSTACK_H
#define __FIXEDSTACK_H

template <class T>
class FixedStack : public Stack<T>
{
private:
    T* arr;
    size_t _size;
    const size_t max_size;

public:
    FixedStack(const int size);
    FixedStack(const FixedStack& src);
    ~FixedStack();

    FixedStack& operator=(FixedStack& src);

    void push(const T& elem) override;

    T pop() override;

    bool isEmpty() override;
    bool isFull();

    const size_t& getSize() const;
};

#endif // __FIXEDSTACK_H


template<class T>
FixedStack<T>::FixedStack(const int size) : max_size(size), _size(0)
{
    arr = nullptr;
    try
    {
        if (max_size >= 0)
        {
            arr = new T[max_size];
        }
        else
        {
            throw WrongStackSize("WrongStackSize: negative size\n");
        }
    }
    catch (WrongStackSize exc)
    {
        std::cerr << exc.what();
    }
}

template <class T>
FixedStack<T>::FixedStack(const FixedStack& src) : max_size(src.max_size), _size(src._size)
{
    arr = nullptr;
    try
    {
        if (max_size >= 0)
        {
            arr = new T[max_size];
            for (int i = 0; i < _size; ++i)
            {
                arr[i] = src.arr[i];
            }
        }
        else
        {
            throw WrongStackSize("WrongStackSize: negative size\n");
        }
    }
    catch (WrongStackSize exc)
    {
        std::cerr << exc.what();
    }
}

template<class T>
FixedStack<T>::~FixedStack()
{
    if (!isEmpty())
    {
        delete[] arr;
    }
}

template <class T>
void FixedStack<T>::push(const T& elem)
{
    try
    {
        if (isFull())
        {
            throw StackOverflow("StackOverflow: pushing into full stack\n");
        }
        else 
        {
            arr[_size] = elem;
            ++_size;
        }
    }
    catch (StackOverflow exc)
    {
        std::cerr << exc.what();
    }
}

template <class T>
T FixedStack<T>::pop()
{
    try
    {
        if (isEmpty())
        {
            throw StackUnderflow("StackUnderflow: poping from empty stack\n");
        }
        else 
        {
            --_size;
            return arr[_size];
        }
    }
    catch (StackUnderflow exc)
    {
        std::cout << exc.what();
    }
    return NULL;
}

template<class T>
FixedStack<T>& FixedStack<T>::operator=(FixedStack<T>& src)
{
    if (this == src) return this;
    arr = nullptr;
    max_size = src.max_size;
    _size = src._size;
    for (int i = 0; i < _size; ++i)
    {
        arr[i] = src.arr[i];
    }
    return this;
}

template<class T>
bool FixedStack<T>::isEmpty()
{
    // checking if stack is empty
    if (_size == 0) return true;

    return false;
}

template<class T>
bool FixedStack<T>::isFull()
{
    if (_size == max_size) return true;

    return false;
}

template<class T>
const size_t& FixedStack<T>::getSize() const
{
    return _size;
}