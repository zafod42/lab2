#include <iostream>
#include <fstream>
#include <string>
#include <regex>

#include "Queue/fixedQueue.hpp"
#include "Stack/fixedStack.hpp"
#include "Brackets/checkBrackets.h"

#define NUMBER_OF_TESTS 13

int main()
{
    std::regex check(R"(([1-9][0-9]*|[0-9]))");
    std::cout << "queue size = 4\n";
    FixedQueue<int> queue(4);
    std::cout << "adding 1, 2, 3, 4, 5\n";
    queue.enQueue(1);
    queue.enQueue(2);
    queue.enQueue(3);
    queue.enQueue(4);
    std::cout << "exception after adding 5:\n";
    queue.enQueue(5);
    std::cout << "is queue: ";
    if (queue.isFull())
    {
        std::cout << "Full\n";
    }
    else
    {
        std::cout << "Not full\n";
    }
    std::cout << "5 is is not added \n";
    std::cout << "1) " << queue.deQueue() << '\n';
    std::cout << "2) " << queue.deQueue() << '\n';
    std::cout << "3) " << queue.deQueue() << '\n';
    std::cout << "4) " << queue.deQueue() << '\n';
    std::cout << "exception after 5th element:\n";
    std::cout << queue.deQueue() << '\n';
    if (queue.isEmpty())
    {
        std::cout << "Empty\n";
    }
    else
    {
        std::cout << "Not empty\n";
    }
    std::cout << "\n\nDemonstrating of circle appendig:\n1, 2, 3, 4\n";
    queue.enQueue(1);
    queue.enQueue(2);
    queue.enQueue(3);
    queue.enQueue(4);
    std::cout << "deQueue firs two:\n";
    std::cout << queue.deQueue() << ' ';
    std::cout << queue.deQueue() << '\n';
    queue.enQueue(5);
    queue.enQueue(6);

    std::cout << "enQueue 5, 6\n";
    std::cout << "deQueue rest:\n";
    std::cout << queue.deQueue() << ' ';
    std::cout << queue.deQueue() << ' ';
    std::cout << queue.deQueue() << ' ';
    std::cout << queue.deQueue() << '\n';

    std::string input = " ";
    std::string size = ".-";
    std::cout << "Enter stack size\n";
    do 
    {
        std::cout << ">>> ";
        std::cin >> size;
        try
        {
            if (std::regex_match(size, check) == false)
            {
                throw std::runtime_error("Error: Positive integer required\n");
            }

        }
        catch (std::runtime_error exc)
        {
            std::cerr << exc.what();
        }
    } while (std::regex_match(size, check) == false);
    FixedStack<int> stack(atoi(size.c_str()));
    std::cout << "enter value to push; enter \"pop\" to pop value; enter \"size\" to see stack size\n>>> ";
    std::cin >> input;
    while (input != "end")
    {
        if (input == "size")
        {
            std::cout << "size = " << stack.getSize() << '\n';
        }
        else if (input == "pop")
        {
            std::cout << "popped: " <<  stack.pop() << '\n';
        }
        else {
            try
            {
                if (std::regex_match(input, check))
                {
                    stack.push(atoi(input.c_str()));
                }
                else
                {
                    throw std::runtime_error("Integer required\n");
                }
            }
            catch (std::runtime_error exc)
            {
                std::cerr << exc.what();
            }
        }
        std::cout << ">>> ";
        std::cin >> input;
    }




    std::fstream in;
    // full path = C:\\Users\\kalab\\OneDrive\\������� ����\\���� ���� 2\\test.txt
    in.open("..\\..\\tests.txt");

    std::string text = " ";
    int deep = 0;

    for (int i = 0; i < NUMBER_OF_TESTS; ++i)
    {
        in >> text;
        in >> deep;
        std::cout << text << " - ";
        if (checkBalanceBrackets(text.c_str(), deep))
        {
            std::cout << "Correct\n";
        }
        else
        {
            std::cout << "Incorrect\n";
        }
    }
    in.close();
    return 0;
}

